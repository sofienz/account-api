# Account-api

## Summary

The assessment consists of an API to be used for opening a new “current account” of already existing customers.

## APIs

```html
POST /v1/account - creates a new account for existing customer
GET /v1/customer/{customerId} - retrieves a customer
GET /v1/customer - retrieves all customers
```
## Tech Stack

- Java 11
- Spring Boot
- Spring Data JPA
- H2 In memory database
- JUnit
- Restful API

## Run 

```ssh

$ mvn clean install
$ mvn spring-boot:run

```

