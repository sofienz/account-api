package com.example.account.service;
import com.example.account.AccountApplicationTests;
import com.example.account.constants.CustomerConstants;
import com.example.account.exception.CustomerNotFoundException;
import com.example.account.model.Customer;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;



@SpringBootTest(classes = AccountApplicationTests.class)
public class CustomerServiceTest {

    @Autowired
    private CustomerService customerService;

    @Test
    public void testFindByCustomerId_whenCustomerIdExists_shouldReturnCustomer(){
        Customer result = customerService.findCustomerById(1);

        assertEquals(result.getName(),
                CustomerConstants.CUSTOMER_1_Name);
    }
    @Test
    public void testFindByCustomerId_whenCustomerIdDoesNotExist_shouldThrowCustomerNotFoundException(){
        assertThrows(CustomerNotFoundException.class, () -> customerService.findCustomerById(5));
    }


}
