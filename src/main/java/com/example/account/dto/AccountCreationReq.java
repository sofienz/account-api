package com.example.account.dto;
import javax.validation.constraints.Min;
import java.math.BigDecimal;

public class AccountCreationReq {

    @Min(value = 1,message = "Field customer Id is required")
    private int customerId;
    @Min(value = 0,message = "Initial Credit cannot be negative")
    private BigDecimal initialCredit;

    public int getCustomerId() {
        return customerId;
    }

    public BigDecimal getInitialCredit() {
        return initialCredit;
    }
}
