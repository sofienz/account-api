package com.example.account;

import com.example.account.constants.CustomerConstants;
import com.example.account.model.Customer;
import com.example.account.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccountApplication implements CommandLineRunner {
	@Autowired
	private CustomerRepository customerRepository;

	public static void main(String[] args) {
		SpringApplication.run(AccountApplication.class, args);

	}

	@Override
	public void run(String... args) throws Exception {
		Customer customer1 =new Customer(CustomerConstants.CUSTOMER_1_Name,"Zribi");
		customerRepository.save(customer1);
		Customer customer2 =new Customer("Mark","Peter");
		customerRepository.save(customer2);

	}
}
