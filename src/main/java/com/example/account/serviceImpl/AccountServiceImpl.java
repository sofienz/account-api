package com.example.account.serviceImpl;

import com.example.account.dto.AccountCreationReq;
import com.example.account.model.Account;
import com.example.account.model.Customer;
import com.example.account.repository.AccountRepository;
import com.example.account.service.AccountService;
import com.example.account.service.CustomerService;
import com.example.account.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;

@Service
public class AccountServiceImpl implements AccountService {
    @Autowired
    private CustomerService customerService;

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private TransactionService transactionService;

    @Override

    public Account createAccount(AccountCreationReq accountCreationReq) {
        Customer customer=customerService.findCustomerById(accountCreationReq.getCustomerId());
        Account account = new Account(accountCreationReq.getInitialCredit(),new Date(),customer);
        Account createdAccount=accountRepository.save(account);
        if (accountCreationReq.getInitialCredit().compareTo(BigDecimal.ZERO) > 0) {
            transactionService.createTransaction(accountCreationReq.getInitialCredit(),createdAccount);
        }

        return createdAccount;
    }

}
