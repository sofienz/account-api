package com.example.account.serviceImpl;

import com.example.account.exception.CustomerNotFoundException;
import com.example.account.model.Customer;
import com.example.account.repository.CustomerRepository;
import com.example.account.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public CustomerServiceImpl() {
    }
    @Override
    public Customer findCustomerById(int id) {
        return customerRepository.findById(id)
                .orElseThrow(
                        () -> new CustomerNotFoundException(id));
    }
    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.findAll();
    }
}
