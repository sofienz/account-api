package com.example.account.serviceImpl;

import com.example.account.model.Account;
import com.example.account.model.Transaction;
import com.example.account.repository.TransactionRepository;
import com.example.account.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Override
    public Transaction createTransaction(BigDecimal amount, Account account) {
        Transaction transaction = new Transaction(
                amount,
                new Date(),
                account);
        return transactionRepository.save(transaction);
    }
}
