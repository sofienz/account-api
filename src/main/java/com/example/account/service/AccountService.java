package com.example.account.service;
import com.example.account.dto.AccountCreationReq;
import com.example.account.model.Account;

public interface AccountService {
    /**
     * Create an account
     * @param accountCreationReq Account info
     * @return The created Account
     */
    public Account createAccount(AccountCreationReq accountCreationReq);

}
