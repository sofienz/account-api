package com.example.account.service;

import com.example.account.model.Account;
import com.example.account.model.Transaction;
import java.math.BigDecimal;

public interface TransactionService {
    /**
     * Creates a Transaction
     * @param amount Transaction amount
     * @param account Account that made the transaction
     * @return The created Transaction
     */
    public Transaction createTransaction(BigDecimal amount, Account account);

}
