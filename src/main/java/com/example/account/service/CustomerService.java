package com.example.account.service;

import com.example.account.model.Customer;
import java.util.List;

public interface CustomerService {
    /**
     * Finds Customer by Id
     * @param id Customer Id
     * @return Customer
     */
    public Customer findCustomerById(int id) ;

    /**
     * Finds all customers
     * @return List of Customers
     */
    public List<Customer> getAllCustomer();


}
